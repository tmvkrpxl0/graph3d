#![feature(new_uninit)]
#![feature(iter_collect_into)]
#![feature(array_windows)]

use std::cmp::min;
use std::f32::consts;
use std::iter::once;
use std::mem::size_of;
use std::ops::{Deref, Range};
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};

use bytemuck::{cast_slice, Pod, Zeroable};
use eframe::App;
use eframe::egui::Context;
use eframe::egui_wgpu::Renderer;
use eframe::egui_wgpu::renderer::ScreenDescriptor;
use nalgebra::{clamp, Matrix3, Matrix4, Perspective3, Rotation, Rotation2, Rotation3, RowVector2, RowVector3, RowVector4, Translation3, Vector3};
use rayon::prelude::*;
use wgpu::{AddressMode, Backends, BindGroup, BindGroupDescriptor, BindGroupEntry, BindGroupLayoutDescriptor, BindGroupLayoutEntry, BindingResource, BindingType, BlendState, Buffer, BufferBinding, BufferBindingType, BufferDescriptor, BufferUsages, Color, ColorTargetState, CommandEncoderDescriptor, CompareFunction, ComputePassDescriptor, ComputePipeline, ComputePipelineDescriptor, DepthStencilState, Device, DeviceDescriptor, Extent3d, Features, FragmentState, FrontFace, include_wgsl, IndexFormat, Instance, InstanceDescriptor, LoadOp, MultisampleState, Operations, PipelineLayoutDescriptor, PrimitiveState, PrimitiveTopology, Queue, RenderPassColorAttachment, RenderPassDepthStencilAttachment, RenderPassDescriptor, RenderPipeline, RenderPipelineDescriptor, RequestAdapterOptions, Sampler, SamplerDescriptor, ShaderStages, Surface, SurfaceConfiguration, SurfaceError, Texture, TextureDescriptor, TextureDimension, TextureUsages, TextureView, VertexAttribute, VertexBufferLayout, VertexState, VertexStepMode};
use wgpu::PolygonMode::Fill;
use wgpu::TextureFormat::Depth32Float;
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use winit::dpi::{PhysicalPosition, PhysicalSize};
use winit::event::{ElementState, Event, VirtualKeyCode, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::{Fullscreen, Window, WindowBuilder};

#[cfg(test)]
mod test;

type WorldMap = [[[u8; 1024]; 1024]; 1024];
type FlatMap = [u8; 1024 * 1024 * 1024];

static COMPUTE_VERTICES: ComputePassDescriptor = ComputePassDescriptor {
    label: Some("Vertex Generator"),
};

pub struct Graph3D {
    last_mouse_pos: PhysicalPosition<f64>,
    speed: f32,
    map: Box<WorldMap>,
    voxels: u32,
    has_changed: bool,
    cam_pos: RowVector3<f32>,
    ratio: f32,

    pitch: f32,
    yaw: f32,
    grab_mouse: bool,

    surface: Surface,
    device: Device,
    queue: Queue,
    config: SurfaceConfiguration,
    size: PhysicalSize<u32>,
    window: Window,

    egui_state: egui_winit::State,
    egui_context: Context,
    egui_renderer: Renderer,

    screen: ScreenDescriptor,
    c_pipeline: ComputePipeline,
    v_pipeline: RenderPipeline,
    request_buffer: Buffer,
    camera_buffer: Buffer,
    generated_vertices: Buffer,
    generated_indices: Buffer,
    vertices: Buffer,
    indices: Buffer,
    group0: BindGroup,
    group1: BindGroup,
    depth: (Texture, TextureView, Sampler),

    inputs: [bool; 6] // wasd space shift
}

impl Graph3D {
    pub async fn new(window: Window, event_loop: &EventLoop<()>) -> Graph3D {
        let size = window.inner_size();
        let instance = Instance::new(InstanceDescriptor {
            backends: Backends::all(),
            dx12_shader_compiler: Default::default(),
        });

        let surface = unsafe { instance.create_surface(&window) }.unwrap();

        let adapter = instance.request_adapter(&RequestAdapterOptions {
            power_preference: Default::default(),
            force_fallback_adapter: false,
            compatible_surface: Some(&surface),
        }).await.unwrap();

        let (device, queue) = adapter.request_device(
            &DeviceDescriptor {
                label: Some("Graph3D"),
                features: Features::empty() | Features::DEPTH32FLOAT_STENCIL8,
                limits: if cfg!(target_arch = "wasm32") {
                    wgpu::Limits::downlevel_webgl2_defaults()
                } else {
                    wgpu::Limits::default()
                },
            },
            None,
        ).await.unwrap();
        ;
        let capabilities = surface.get_capabilities(&adapter);
        let format = capabilities.formats.iter()
            .find(|f| f.is_srgb())
            .copied()
            .unwrap_or(capabilities.formats[0]);

        let config = SurfaceConfiguration {
            usage: TextureUsages::RENDER_ATTACHMENT,
            format,
            width: size.width,
            height: size.height,
            present_mode: capabilities.present_modes[0],
            alpha_mode: capabilities.alpha_modes[0],
            view_formats: vec![],
        };
        surface.configure(&device, &config);

        let shader = device.create_shader_module(include_wgsl!("shader.wgsl"));
        let request_buffer = device.create_buffer(&BufferDescriptor {
            label: Some("Request Buffer"),
            usage: BufferUsages::STORAGE | BufferUsages::COPY_DST,
            size: (size_of::<u32>() * 4098) as u64,
            mapped_at_creation: false,
        });
        let camera_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Camera Buffer"),
            contents: cast_slice(&[0.0; 16]),
            usage: BufferUsages::UNIFORM | BufferUsages::COPY_DST,
        });
        let generated_vertices = device.create_buffer(&BufferDescriptor {
            label: Some("Generated Vertices Buffer"),
            usage: BufferUsages::STORAGE | BufferUsages::COPY_DST | BufferUsages::COPY_SRC,
            size: size_of::<[[i32; 4]; 4096]>() as u64,
            mapped_at_creation: false,
        });
        let generated_indices = device.create_buffer(&BufferDescriptor {
            label: Some("Generated Indices Buffer"),
            usage: BufferUsages::STORAGE | BufferUsages::COPY_DST | BufferUsages::COPY_SRC,
            size: size_of::<[[u32; 36]; 4096]>() as u64,
            mapped_at_creation: false,
        });

        let vertices = device.create_buffer(&BufferDescriptor {
            label: Some("Vertices Buffer"),
            usage: BufferUsages::VERTEX | BufferUsages::COPY_DST,
            size: size_of::<[[i32; 4]; 4096]>() as u64,
            mapped_at_creation: false,
        });
        let indices = device.create_buffer(&BufferDescriptor {
            label: Some("Indices Buffer"),
            usage: BufferUsages::INDEX | BufferUsages::COPY_DST,
            size: size_of::<[[u32; 36]; 4096]>() as u64,
            mapped_at_creation: false,
        });

        let group0_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: Some("group0"),
            entries: &[
                BindGroupLayoutEntry {
                    binding: 0,
                    visibility: ShaderStages::COMPUTE,
                    ty: BindingType::Buffer {
                        ty: BufferBindingType::Storage {
                            read_only: true,
                        },
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                },
                BindGroupLayoutEntry {
                    binding: 1,
                    visibility: ShaderStages::VERTEX | ShaderStages::COMPUTE,
                    ty: BindingType::Buffer {
                        ty: BufferBindingType::Uniform {},
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }
            ],
        });

        let group1_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: Some("group1"),
            entries: &[
                BindGroupLayoutEntry {
                    binding: 0,
                    visibility: ShaderStages::COMPUTE,
                    ty: BindingType::Buffer {
                        ty: BufferBindingType::Storage { read_only: false },
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                },
                BindGroupLayoutEntry {
                    binding: 1,
                    visibility: ShaderStages::COMPUTE,
                    ty: BindingType::Buffer {
                        ty: BufferBindingType::Storage { read_only: false },
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }
            ],
        });

        let group0 = device.create_bind_group(&BindGroupDescriptor {
            label: Some("group0"),
            layout: &group0_layout,
            entries: &[
                BindGroupEntry {
                    binding: 0,
                    resource: BindingResource::Buffer(BufferBinding {
                        buffer: &request_buffer,
                        offset: 0,
                        size: None,
                    }),
                },
                BindGroupEntry {
                    binding: 1,
                    resource: BindingResource::Buffer(BufferBinding {
                        buffer: &camera_buffer,
                        offset: 0,
                        size: None,
                    }),
                }
            ],
        });

        let group1 = device.create_bind_group(&BindGroupDescriptor {
            label: Some("group1"),
            layout: &group1_layout,
            entries: &[
                BindGroupEntry {
                    binding: 0,
                    resource: BindingResource::Buffer(BufferBinding {
                        buffer: &generated_vertices,
                        offset: 0,
                        size: None,
                    }),
                },
                BindGroupEntry {
                    binding: 1,
                    resource: BindingResource::Buffer(BufferBinding {
                        buffer: &generated_indices,
                        offset: 0,
                        size: None,
                    }),
                }
            ],
        });

        let v_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: Some("Graph3D Render Pipeline Layout"),
            bind_group_layouts: &[&group0_layout, &group1_layout],
            push_constant_ranges: &[],
        });
        let c_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: Some("Graph3D Compute Pipeline Layout"),
            bind_group_layouts: &[&group0_layout, &group1_layout],
            push_constant_ranges: &[],
        });

        let c_pipeline = device.create_compute_pipeline(&ComputePipelineDescriptor {
            label: Some("Graph3D Compute Pipeline"),
            layout: Some(&c_layout),
            module: &shader,
            entry_point: "compute_main",
        });
        let v_pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
            label: Some("Graph3D Render Pipeline"),
            layout: Some(&v_layout),
            vertex: VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[VertexInput::desc()],
            },
            primitive: PrimitiveState {
                topology: PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: FrontFace::Ccw,
                cull_mode: None,
                unclipped_depth: false,
                polygon_mode: Fill,
                conservative: false,
            },
            depth_stencil: Some(DepthStencilState {
                format: Depth32Float,
                depth_write_enabled: true,
                depth_compare: CompareFunction::Less,
                stencil: Default::default(),
                bias: Default::default(),
            }),
            multisample: MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            fragment: Some(FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[Some(ColorTargetState {
                    format,
                    blend: Some(BlendState::REPLACE),
                    write_mask: Default::default(),
                })],
            }),
            multiview: None,
        });

        let egui_context = Context::default();
        let egui_state = egui_winit::State::new(event_loop);
        let egui_renderer = Renderer::new(&device, format, None, 1);

        let screen = ScreenDescriptor {
            size_in_pixels: [config.width, config.height],
            pixels_per_point: egui_context.pixels_per_point(),
        };

        let depth = Graph3D::create_depth(&device, &config);

        let mut map: Box<WorldMap> = unsafe { Box::new_zeroed().assume_init() };

        fill_map(map.as_mut());

        Self {
            last_mouse_pos: Default::default(),
            speed: 1.0 / 60.0,
            map,
            voxels: 1,
            has_changed: true,
            cam_pos: RowVector3::new(0.0, 0.0, 0.0),
            ratio: 1.0,
            pitch: 0.0,
            yaw: 0.0,
            grab_mouse: false,
            surface,
            device,
            queue,
            config,
            size,
            window,
            egui_state,
            egui_context,
            egui_renderer,
            screen,
            c_pipeline,
            v_pipeline,
            request_buffer,
            camera_buffer,
            generated_vertices,
            generated_indices,
            vertices,
            indices,
            group0,
            group1,
            depth,
            inputs: [false; 6],
        }
    }

    pub fn generate_request(&mut self) -> Vec<u32> {
        let (tx, rx): (Sender<u32>, Receiver<u32>) = mpsc::channel();
        self.map.par_iter().enumerate().for_each_with(tx, |sender, (x, plane)| {
            plane.iter().enumerate().for_each(|(y, line)| {
                line.iter().enumerate().for_each(|(z, value)| {
                    if *value == 0xFF {
                        let coordinate = (x << 20) | (y << 10) | z;
                        sender.send(coordinate as u32).unwrap();
                    }
                })
            })
        });

        let mut large_map = Vec::with_capacity(4098);
        rx.into_iter().collect_into(&mut large_map);
        self.voxels = large_map.len() as u32;

        assert!(self.voxels <= 4096);
        dbg!(self.voxels);
        while large_map.len() < 4096 {
            large_map.push(0);
        }
        large_map.push(self.voxels as u32); // index 4096 -> voxel count
        large_map.push(0); // index 4097 -> offset
        large_map
    }

    pub fn update(&mut self) -> Result<(), SurfaceError> {
        {
            let (front, right) = {
                let rotation = Rotation::from_axis_angle(&Vector3::y_axis(), -self.yaw.to_radians());
                (RowVector3::new(0.0, 0.0, self.speed) * rotation, RowVector3::new(self.speed, 0.0, 0.0) * rotation)
            };
            if self.inputs[0] {
                self.cam_pos += front;
            }
            if self.inputs[1] {
                self.cam_pos -= right;
            }
            if self.inputs[2] {
                self.cam_pos -= front;
            }
            if self.inputs[3] {
                self.cam_pos += right;
            }
            if self.inputs[4] {
                self.cam_pos.y += self.speed;
            }
            if self.inputs[5] {
                self.cam_pos.y -= self.speed;
            }
        }

        let output = self.surface.get_current_texture()?;

        let view = output.texture.create_view(&Default::default());
        let mut encoder = self.device.create_command_encoder(&ENCODER);
        if self.has_changed {
            {
                let mut request = self.generate_request();
                let mut pass = encoder.begin_compute_pass(&COMPUTE_VERTICES);
                pass.set_pipeline(&self.c_pipeline);
                pass.set_bind_group(0, &self.group0, &[]);
                pass.set_bind_group(1, &self.group1, &[]);

                let mut remaining = request[4096];

                //while remaining > 0 {
                    let batch = min(remaining, 256);
                    self.queue.write_buffer(&self.request_buffer, 0, cast_slice(request.as_slice()));
                    pass.dispatch_workgroups(1, 1, 1);

                    request[4097] += batch;
                    remaining -= batch;
                //}
            }
            encoder.copy_buffer_to_buffer(&self.generated_vertices, 0, &self.vertices, 0, self.generated_vertices.size());
            encoder.copy_buffer_to_buffer(&self.generated_indices, 0, &self.indices, 0, self.generated_indices.size());

            self.has_changed = false;
        }

        let matrix = evaluate_vp_matrix(self.ratio, &self.cam_pos, self.pitch, self.yaw);
        self.queue.write_buffer(&self.camera_buffer, 0, cast_slice(&matrix.data.0));

        unsafe {
            let mut render_pass = encoder.begin_render_pass(&RenderPassDescriptor {
                label: Some("Graph3D Render Pass"),
                color_attachments: &[Some(RenderPassColorAttachment {
                    view: &view,
                    resolve_target: None,
                    ops: Operations {
                        load: LoadOp::Clear(Color {
                            r: 0.2,
                            g: 0.2,
                            b: 0.2,
                            a: 1.0,
                        }),
                        store: true,
                    },
                })],
                depth_stencil_attachment: Some(RenderPassDepthStencilAttachment {
                    view: &self.depth.1,
                    depth_ops: Some(Operations {
                        load: LoadOp::Clear(1.0),
                        store: true,
                    }),
                    stencil_ops: None,
                }),
            });
            render_pass.set_pipeline(&self.v_pipeline);
            render_pass.set_bind_group(0, &self.group0, &[]);
            render_pass.set_bind_group(1, &self.group1, &[]);
            render_pass.set_index_buffer(self.indices.slice(..), IndexFormat::Uint32);
            render_pass.set_vertex_buffer(0, self.vertices.slice(..));

            let view_direction = view_vector(self.pitch, self.yaw);

            let up = RowVector3::new(0.0, 1.0, 0.0).dot(&view_direction).is_sign_negative();
            let down = RowVector3::new(0.0, -1.0, 0.0).dot(&view_direction).is_sign_negative();
            let left = RowVector3::new(-1.0, 0.0, 0.0).dot(&view_direction).is_sign_negative();
            let right = RowVector3::new(1.0, 0.0, 0.0).dot(&view_direction).is_sign_negative();
            let front = RowVector3::new(0.0, 0.0, -1.0).dot(&view_direction).is_sign_negative();
            let back = RowVector3::new(0.0, 0.0, 1.0).dot(&view_direction).is_sign_negative();

            let curle = [up, down, left, right, front, back];

            for i in 0..6u32 {
                if curle[i as usize] {
                    render_pass.draw_indexed((self.voxels * i * 6)..(self.voxels * (i + 1) * 6), 0, 0..1);
                }
            }
        }
        self.queue.submit(once(encoder.finish()));
        output.present();

        Ok(())
    }

    fn create_depth(device: &Device, config: &SurfaceConfiguration) -> (Texture, TextureView, Sampler) {
        let depth_texture = device.create_texture(&TextureDescriptor {
            label: Some("Graph3D Depth Texture"),
            size: Extent3d {
                width: config.width,
                height: config.height,
                depth_or_array_layers: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: Depth32Float,
            usage: TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING,
            view_formats: &[],
        });
        let depth_view = depth_texture.create_view(&Default::default());
        let depth_sampler = device.create_sampler(&SamplerDescriptor {
            label: Some("Graph3D Depth Sampler"),
            address_mode_u: AddressMode::ClampToEdge,
            address_mode_v: AddressMode::ClampToEdge,
            address_mode_w: AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Nearest,
            lod_min_clamp: 0.0,
            lod_max_clamp: 100.0,
            compare: Some(CompareFunction::Less),
            ..Default::default()
        });

        (depth_texture, depth_view, depth_sampler)
    }

    pub fn resize(&mut self, size: PhysicalSize<u32>) {
        if size.height == 0 || size.width == 0 {
            return;
        }

        self.size = size;
        self.config.width = size.width;
        self.config.height = size.height;
        self.surface.configure(&self.device, &self.config);

        self.screen.pixels_per_point = self.egui_context.pixels_per_point();
        self.screen.size_in_pixels = [self.config.width, self.config.height];
        self.depth = Graph3D::create_depth(&self.device, &self.config);

        self.ratio = size.width as f32 / size.height as f32;
    }
}

#[cfg_attr(target_arch = "wasm32", wasm_bindgen(start))]
pub async fn run() {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_maximized(false)
        .with_title("Graph3D :D")
        .build(&event_loop)
        .unwrap();
    let mut graph3d = Graph3D::new(window, &event_loop).await;

    event_loop.run(move |event, _, control_flow| {
        match event {
            Event::RedrawRequested(window_id) => if window_id == graph3d.window.id() {
                match graph3d.update() {
                    Ok(_) => {}
                    Err(SurfaceError::Lost) => graph3d.resize(graph3d.size),
                    Err(SurfaceError::OutOfMemory) => control_flow.set_exit(),
                    Err(e) => eprintln!("{:?}", e),
                };
            }
            Event::WindowEvent { ref event, window_id } if window_id == graph3d.window.id() => 'window: {
                let response = graph3d.egui_state.on_event(&graph3d.egui_context, event);
                if response.repaint {
                    graph3d.window.request_redraw();
                }
                if response.consumed {
                    break 'window;
                };

                match event {
                    WindowEvent::Resized(new_size) => graph3d.resize(*new_size),
                    WindowEvent::ScaleFactorChanged { new_inner_size, .. } => graph3d.resize(**new_inner_size),
                    WindowEvent::CloseRequested => *control_flow = ControlFlow::ExitWithCode(0),
                    WindowEvent::KeyboardInput { input, .. } => 'key: {
                        let pressed = matches!(input.state, ElementState::Pressed);
                        let Some(code) = input.virtual_keycode else { break 'key; };

                        match code {
                            VirtualKeyCode::Escape => *control_flow = ControlFlow::ExitWithCode(0),
                            VirtualKeyCode::W => {
                                graph3d.inputs[0] = pressed;
                            }
                            VirtualKeyCode::A => {
                                graph3d.inputs[1] = pressed;
                            }
                            VirtualKeyCode::S => {
                                graph3d.inputs[2] = pressed;
                            }
                            VirtualKeyCode::D => {
                                graph3d.inputs[3] = pressed;
                            }
                            VirtualKeyCode::Space => {
                                graph3d.inputs[4] = pressed;
                            }
                            VirtualKeyCode::LShift => {
                                graph3d.inputs[5] = pressed;
                            }
                            VirtualKeyCode::C if pressed => {
                                graph3d.grab_mouse = !graph3d.grab_mouse;
                            }
                            VirtualKeyCode::V if pressed => {
                                graph3d.has_changed = true;
                            }
                            VirtualKeyCode::F if pressed => {
                                match graph3d.window.fullscreen() {
                                    None => {
                                        graph3d.window.set_fullscreen(Some(Fullscreen::Borderless(graph3d.window.current_monitor())))
                                    }
                                    Some(_) => {
                                        graph3d.window.set_fullscreen(None)
                                    }
                                };
                            }
                            VirtualKeyCode::R if pressed => {
                                graph3d.pitch = 0.0;
                                graph3d.yaw = 0.0;

                                graph3d.cam_pos.x = 0.0;
                                graph3d.cam_pos.y = 0.0;
                                graph3d.cam_pos.z = 0.0;
                            }
                            _ => {}
                        };
                    }
                    WindowEvent::CursorMoved { position, .. } => {
                        let delta_x = position.x - graph3d.last_mouse_pos.x;
                        let delta_y = position.y - graph3d.last_mouse_pos.y;
                        let screen_size = graph3d.size;

                        let yaw_diff = 180.0 * (delta_x / screen_size.width as f64);
                        let pitch_diff = 90.0 * (delta_y / screen_size.height as f64);

                        graph3d.pitch += pitch_diff as f32;
                        graph3d.yaw += yaw_diff as f32;

                        graph3d.pitch = clamp(graph3d.pitch, -90.0, 90.0);
                        graph3d.yaw = (graph3d.yaw + 360.0) % 360.0;

                        if graph3d.grab_mouse {
                            let center = PhysicalPosition::new(screen_size.width / 2, screen_size.height / 2);
                            if let Err(e) = graph3d.window.set_cursor_position(center) {
                                eprintln!("{e}");
                            }

                            graph3d.last_mouse_pos.x = center.x as f64;
                            graph3d.last_mouse_pos.y = center.y as f64;
                        } else {
                            graph3d.last_mouse_pos.x = position.x;
                            graph3d.last_mouse_pos.y = position.y;
                        }
                    }
                    WindowEvent::MouseWheel { .. } => {}
                    WindowEvent::MouseInput { .. } => {}
                    _ => {}
                }
            }
            Event::MainEventsCleared => graph3d.window.request_redraw(),
            _ => {}
        }
    });
}

fn view_vector(mut pitch: f32, mut yaw: f32) -> RowVector3<f32> {
    yaw -= 90.0;
    yaw *= -1.0;
    pitch *= -1.0;

    let yaw_rad = yaw.to_radians();
    let pitch_rad = pitch.to_radians();
    let view_x = yaw_rad.cos() * pitch_rad.cos();
    let view_y = pitch_rad.sin();
    let view_z = yaw_rad.sin() * pitch_rad.cos();

    let view_length = (view_x * view_x + view_y * view_y + view_z * view_z).sqrt();
    let normalized_view_x = view_x / view_length;
    let normalized_view_y = view_y / view_length;
    let normalized_view_z = view_z / view_length;
    RowVector3::new(normalized_view_x, normalized_view_y, normalized_view_z)
}

fn rotation(pitch: f32, yaw: f32) -> Matrix3<f32> {
    Rotation3::from_euler_angles(-pitch.to_radians(), -yaw.to_radians(), 0.0).into() // Axis system is different
}

fn evaluate_vp_matrix(ratio: f32, camera_pos: &RowVector3<f32>, pitch: f32, yaw: f32) -> Matrix4<f32> {
    let view_matrix = {
        let translation = Translation3::new(-camera_pos.x, -camera_pos.y, -camera_pos.z).to_homogeneous().transpose();

        translation * rotation(-pitch, -yaw).to_homogeneous()
    };

    let mut flip = Matrix4::<f32>::identity();
    flip.m33 = -1.0;
    let projection_matrix = Perspective3::new(ratio, 90.0, 0.01, 100.0).into_inner().transpose();

    view_matrix * flip * projection_matrix
}

static ENCODER: CommandEncoderDescriptor = CommandEncoderDescriptor {
    label: Some("Graph3D Commands"),
};

// Copy of wgsl struct VertexInput
#[derive(Copy, Clone, Debug)]
#[repr(C)]
pub struct VertexInput {
    position: RowVector4<i32>,
}

impl VertexInput {
    const ATTRIBS: [VertexAttribute; 1] =
        wgpu::vertex_attr_array![0 => Sint32x4];

    fn desc() -> VertexBufferLayout<'static> {
        VertexBufferLayout {
            array_stride: size_of::<[i32; 4]>() as u64,
            step_mode: VertexStepMode::Vertex,
            attributes: &Self::ATTRIBS,
        }
    }
}

/*fn fill_map(map: &mut WorldMap) {
    for x in 0..256 {
        for y in 0..4 {
            map[x + 512][y + 512][512] = 0xFF;
        }
    }
}*/

// 420.5 * 2 * sqrt(2) * pi
fn fill_map(map: &mut WorldMap) {
    for x in 0..10 {
        let vec = RowVector2::new(x as f32, x as f32);
        for i in 0..200 {
            let angle: f32 = consts::PI * ((i as f32) / 100.0);

            let rotation = Rotation2::new(angle);
            let rotated = vec * rotation;

            dbg!(&rotated);

            let y = (rotated.data.0[0][0] + 512.0) as usize;
            let z = (rotated.data.0[1][0] + 512.0) as usize;

            map[x + 512][y][z] = 0xFF;
        }
    }
}

fn function(y: f32, z: f32) -> bool {
    y.floor() == z.floor()
}
