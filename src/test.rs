use nalgebra::{Rotation, RowVector3, Vector};

#[test]
fn test_rotation() {
    let view_vec = RowVector3::new(0.0, 0.0, 1.0) * Rotation::from_axis_angle(&Vector::y_axis(), 270.0_f32.to_radians());
    dbg!(view_vec);
}
