#![feature(new_uninit)]

use pollster::block_on;
use graph3d::run;

fn main() {
    block_on(run())
}
