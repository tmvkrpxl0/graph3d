// w is 1 for position
// w is 0 for direction

struct VertexGenRequest {
    voxels: array<u32, 4096>,
    // MSB UUXXXXXXXXXXYYYYYYYYYYZZZZZZZZZZ LSB
    voxel_count: u32, // voxels is fixed length array and there mabe less than 256. This can be bigger than 256 for multiple request
    offset: u32 // I may sent multiple requests
}

@group(0) @binding(0)
var<storage, read> request: VertexGenRequest;

struct Camera {
    view_proj: mat4x4<f32>,
};
@group(0) @binding(1)
var<uniform> camera: Camera;

// I hate 16 alignment rule
// Below 2 are generated on compute shader
@group(1) @binding(0)
var<storage, read_write> generated_vertices: array<vec4<i32>, 32768/*1073741824*/>;
@group(1) @binding(1)
var<storage, read_write> generated_indices: array<u32, 147456/*4831838208*/>;

fn unpack(packed: u32) -> vec3<i32> {
    let z = i32(packed & 1023u);
    let y = i32((packed & 1047552u) >> 10u);
    let x = i32((packed & 1072693248u) >> 20u);

    return vec3(x, y, z);
}

@compute
@workgroup_size(256, 1, 1)
fn compute_main(@builtin(global_invocation_id) global_id: vec3<u32>) {
    let id = global_id.x + request.offset; // simulate global id being bigger than 256
    if (request.voxel_count <= id) {
        return;
    }

    let pos = unpack(request.voxels[id]);

    let v_buffer_start = id * 8u;

    let up_start = (request.voxel_count * 0u + id) * 6u;
    let down_start = (request.voxel_count * 1u + id) * 6u;
    let left_start = (request.voxel_count * 2u + id) * 6u;
    let right_start = (request.voxel_count * 3u + id) * 6u;
    let front_start = (request.voxel_count * 4u + id) * 6u;
    let back_start = (request.voxel_count * 5u + id) * 6u;

    let a = vec4(pos.x + 0, pos.y + 1, pos.z + 0, 1);
    let b = vec4(pos.x + 1, pos.y + 1, pos.z + 0, 1);
    let c = vec4(pos.x + 1, pos.y + 1, pos.z + 1, 1);
    let d = vec4(pos.x + 0, pos.y + 1, pos.z + 1, 1);
    let e = vec4(pos.x + 0, pos.y + 0, pos.z + 0, 1);
    let f = vec4(pos.x + 1, pos.y + 0, pos.z + 0, 1);
    let g = vec4(pos.x + 1, pos.y + 0, pos.z + 1, 1);
    let h = vec4(pos.x + 0, pos.y + 0, pos.z + 1, 1);

    let a_index = v_buffer_start + 0u;
    let b_index = v_buffer_start + 1u;
    let c_index = v_buffer_start + 2u;
    let d_index = v_buffer_start + 3u;
    let e_index = v_buffer_start + 4u;
    let f_index = v_buffer_start + 5u;
    let g_index = v_buffer_start + 6u;
    let h_index = v_buffer_start + 7u;

    generated_vertices[a_index] = a;
    generated_vertices[b_index] = b;
    generated_vertices[c_index] = c;
    generated_vertices[d_index] = d;
    generated_vertices[e_index] = e;
    generated_vertices[f_index] = f;
    generated_vertices[g_index] = g;
    generated_vertices[h_index] = h;

    generated_indices[up_start + 0u] = a_index;
    generated_indices[up_start + 1u] = b_index;
    generated_indices[up_start + 2u] = d_index;

    generated_indices[up_start + 3u] = b_index;
    generated_indices[up_start + 4u] = c_index;
    generated_indices[up_start + 5u] = d_index;

    generated_indices[down_start + 0u] = e_index;
    generated_indices[down_start + 1u] = h_index;
    generated_indices[down_start + 2u] = f_index;

    generated_indices[down_start + 3u] = f_index;
    generated_indices[down_start + 4u] = h_index;
    generated_indices[down_start + 5u] = g_index;

    generated_indices[left_start + 0u] = a_index;
    generated_indices[left_start + 1u] = d_index;
    generated_indices[left_start + 2u] = e_index;

    generated_indices[left_start + 3u] = e_index;
    generated_indices[left_start + 4u] = d_index;
    generated_indices[left_start + 5u] = h_index;

    generated_indices[right_start + 0u] = b_index;
    generated_indices[right_start + 1u] = f_index;
    generated_indices[right_start + 2u] = c_index;

    generated_indices[right_start + 3u] = c_index;
    generated_indices[right_start + 4u] = f_index;
    generated_indices[right_start + 5u] = g_index;

    generated_indices[front_start + 0u] = a_index;
    generated_indices[front_start + 1u] = b_index;
    generated_indices[front_start + 2u] = e_index;

    generated_indices[front_start + 3u] = b_index;
    generated_indices[front_start + 4u] = e_index;
    generated_indices[front_start + 5u] = f_index;

    generated_indices[back_start + 0u] = d_index;
    generated_indices[back_start + 1u] = c_index;
    generated_indices[back_start + 2u] = h_index;

    generated_indices[back_start + 3u] = c_index;
    generated_indices[back_start + 4u] = g_index;
    generated_indices[back_start + 5u] = h_index;
}

@vertex
fn vs_main(@location(0) model: vec4<i32>) -> VoxelVertex {
    var position: vec4<f32>;
    position.x = f32(model.x - 512);
    position.y = f32(model.y - 512);
    position.z = f32(model.z - 512);
    position.w = f32(model.w);
    let cloned = vec4<f32>(position);

    position *= camera.view_proj;

    var vertex: VoxelVertex;
    vertex.position = position;
    vertex.world_vertex = cloned;

    return vertex;
}

struct VoxelVertex {
    @builtin(position) position: vec4<f32>,
    @location(0) world_vertex: vec4<f32>
};

fn pick_color(position: vec4<f32>) -> vec4<f32> {
    var close_encounter = 0u;

    if (is_almost_integer(position.x)) {
        close_encounter += 1u;
    }
    if (is_almost_integer(position.y)) {
        close_encounter += 1u;
    }
    if (is_almost_integer(position.z)) {
        close_encounter += 1u;
    }

    if (close_encounter == 1u) {
        return vec4<f32>(0.0, 1.0, 0.0, 1.0);
    } else if (close_encounter == 2u) {
        return vec4<f32>(0.0, 0.0, 1.0, 1.0);
    } else if (close_encounter == 3u) {
        return vec4<f32>(1.0, 0.0, 0.0, 1.0);
    } else {
        return vec4<f32>(1.0, 1.0, 1.0, 1.0);
    }
}

fn is_almost_integer(value: f32) -> bool {
    return abs(round(value) - value) < 0.2;
}

@fragment
fn fs_main(vertex: VoxelVertex) -> @location(0) vec4<f32> {
    let color = pick_color(vertex.world_vertex);
    return color;
}
